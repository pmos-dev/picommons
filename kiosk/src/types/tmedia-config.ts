import { CommonsType } from 'tscommons-core';

export type TMediaConfig = {
		path: string;
};

export function isTMediaConfig(test: any): test is TMediaConfig {
	if (!CommonsType.hasPropertyString(test, 'path')) return false;

	return true;
}
