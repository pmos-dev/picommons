import * as child_process from 'child_process';

import { CommonsDate } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';

export abstract class PiKiosk {
	public static setSystemTime(time: Date): void {
		const date: string = CommonsDate.dateToBashTimestamp(time);
		CommonsOutput.info(`Setting datetime to be: ${date}`);
		child_process.execSync(`sudo date -s "${date}"`);
	}

	public static shutdown(): void {
		CommonsOutput.info(`Forcibly shutting kiosk down`);
		child_process.execSync(`sudo shutdown now`);
	}

	public static reboot(): void {
		CommonsOutput.info(`Forcibly rebooting kiosk`);
		child_process.execSync(`sudo shutdown -r now`);
	}
}
