import { TPropertyObject } from 'tscommons-core';
import { CommonsAsync } from 'tscommons-async';

import { isICommonsExpressConfig } from 'nodecommons-express';
import { CommonsOutput } from 'nodecommons-cli';
import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsAngularApp } from 'nodecommons-app-angular';
import { isICommonsAngularConfig } from 'nodecommons-app-angular';

import { PiChrome } from 'picommons-chrome';

import { PiKioskSystemApi } from '../apis/pi-kiosk-system.api';
import { PiKioskMediaApi } from '../apis/pi-kiosk-media.api';

import { PiKioskSocketIoServer } from '../servers/pi-kiosk-socket-io.server';

import { PiKiosk } from '../helpers/pi-kiosk';

import { isTMediaConfig } from '../types/tmedia-config';

export abstract class PiKioskAngularApp<S extends PiKioskSocketIoServer> extends CommonsAngularApp<S> {
	private chrome: PiChrome|undefined;
	
	constructor(
			name: string,
			strict: boolean = true,
			configFile?: string,
			configPath?: string,	// NB, a direct path, not the name of the args parameter
			envVar?: string,
			argsKey: string = 'config-path',
			private startChrome: boolean = true,
			private shutdownOnExit: boolean = false
	) {
		super(
				name,
				strict,
				configFile,
				configPath,
				envVar,
				argsKey
		);
		
		this.install(
				(restServer: CommonsRestServer, path: string): void => {
					// tslint:disable-next-line:no-unused-expression
					new PiKioskSystemApi(
							restServer,
							path
					);
				}
		);
		
		const mediaConfig: TPropertyObject = this.getConfigArea('media');
		if (!isTMediaConfig(mediaConfig)) throw new Error('Media config is not valid');
		
		this.install(
				(restServer: CommonsRestServer, path: string): void => {
					// tslint:disable-next-line:no-unused-expression
					new PiKioskMediaApi(
							restServer,
							path,
							mediaConfig.path
					);
				}
		);
	}

	protected async init(): Promise<void> {
		await super.init();

		const paths: string[] = [];

		const expressConfig: TPropertyObject = this.getConfigArea('express');
		if (!isICommonsExpressConfig(expressConfig)) throw new Error('Express config is not valid');

		if (expressConfig.path) paths.push(expressConfig.path);

		const angularConfig: TPropertyObject = this.getConfigArea('angular', true);
		if (!isICommonsAngularConfig(angularConfig)) throw new Error('Angular config is not valid');
		
		paths.push(angularConfig.urlPath || 'angular');
		
		if (paths.length > 0 && !/^\//.test(paths[0])) paths.unshift('');
		if (paths.length > 0 && !/\/$/.test(paths[paths.length - 1])) paths.push('');
		
		if (this.startChrome) {
			CommonsOutput.debug(`Creating Chrome UI on URL: http://localhost:${expressConfig.port}${paths.join('/')}`);
			this.chrome = new PiChrome(
					'chromium-browser',
					`http://localhost:${expressConfig.port}${paths.join('/')}`
			);
		}
	}
	
	protected listening(): void {
		// don't use await CommonsAsync.timeout so that the process doesn't block
		setTimeout(
				async (): Promise<void> => {
					if (!this.chrome) return;
				
					CommonsOutput.debug('Opening Chrome UI');
					await this.chrome.launch(true);
				},
				1000
		);
	}
	
	protected async shutdown(): Promise<void> {
		// this needs to happen BEFORE we kill chrome, to allow the socket.io signal to be sent
		await super.shutdown();

		if (this.chrome) {
			// block to allow the socket.io message to be processed
			await CommonsAsync.timeout(1000);
			
			CommonsOutput.debug('Killing Chrome process');
			this.chrome.kill();
		}
		
		// only register the shutdown hook on exit so that aborted apps don't end up shutting down
		if (this.shutdownOnExit) {
			process.on(
					'exit',
					(code: number): void => {
						if (code === 0) PiKiosk.shutdown();
					}
			);
		}
	}
}
