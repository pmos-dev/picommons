import { CommonsExpressServer } from 'nodecommons-express';
import { ICommonsExpressConfig } from 'nodecommons-express';
import { CommonsAngularSocketIoServer } from 'nodecommons-app-angular';

// this is intended to be overriden by classes, but can potentially be just used as is
export class PiKioskSocketIoServer extends CommonsAngularSocketIoServer {
	constructor(
			expressServer: CommonsExpressServer,
			expressConfig: ICommonsExpressConfig
	) {
		super(
				expressServer,
				expressConfig
		);
	}
	
	public async closeUi(): Promise<void> {
		await this.broadcast('ui/close');
	}
	
	public async reload(): Promise<void> {
		await this.broadcast('ui/reload');
	}
}
