import * as express from 'express';

import { CommonsType } from 'tscommons-core';

import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsRestApi } from 'nodecommons-rest';
import { CommonsFile } from 'nodecommons-file';
import { CommonsOutput } from 'nodecommons-cli';

export class PiKioskMediaApi extends CommonsRestApi {
	constructor(
			restServer: CommonsRestServer,
			path: string,
			mediaPath: string
	) {
		super(restServer);
		
		super.getHandler(
				`${path}medias`,
				async (_req: express.Request, _res: express.Response): Promise<string[]> => {
					try {
						return CommonsFile.listDirectories(mediaPath);
					} catch (e) {
						return CommonsRestApi.error(e.message);
					}
				}
		);
		
		super.getHandler(
				`${path}medias/:mount[idname]`,
				async (req: express.Request, _res: express.Response): Promise<string[]> => {
					try {
						const mountPath: string = `${mediaPath}/${req['strictParams']['mount']}`;
						CommonsOutput.debug(`Scanning media ${mountPath}`);

						if (CommonsType.hasPropertyString(req.query, 'extension')) {
							return CommonsFile.listFilesWithExtensions(mountPath, req.query.extension);
						} else {
							return CommonsFile.listFiles(mountPath);
						}
					} catch (e) {
						return CommonsRestApi.error(e.message);
					}
				}
		);
		
		super.deleteHandler(
				`${path}medias/:mount[idname]`,
				async (req: express.Request, _res: express.Response): Promise<void> => {
					try {
						const mountPath: string = `${mediaPath}/${req['strictParams']['mount']}`;
						CommonsOutput.debug(`Unmounting media ${mountPath}`);
						
						CommonsFile.sync();
						CommonsFile.umount(mountPath);
					} catch (e) {
						return CommonsRestApi.error(e.message);
					}
				}
		);
	}
}
