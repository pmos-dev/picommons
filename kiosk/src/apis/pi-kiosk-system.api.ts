import * as express from 'express';

import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { CommonsObject } from 'tscommons-core';

import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsRestApi } from 'nodecommons-rest';
import { CommonsOutput } from 'nodecommons-cli';

import { PiKiosk } from '../helpers/pi-kiosk';

// NB we can't use observables for the various methods as we have to be able to await on them

export class PiKioskSystemApi extends CommonsRestApi {
	constructor(
			restServer: CommonsRestServer,
			path: string
	) {
		super(restServer);
		
		super.patchHandler(
				`${path}system/time`,
				async (req: express.Request, _res: express.Response): Promise<void> => {
					CommonsOutput.debug('REST set time called');
					
					if (!CommonsType.hasPropertyObject(req, 'body')) return CommonsRestApi.badRequest('No body supplied');
					const object: TPropertyObject = CommonsObject.stripNulls(CommonsType.decodePropertyObject(req.body));

					if (!CommonsType.hasPropertyDate(object, 'timestamp')) return CommonsRestApi.badRequest('No state specified');
					const timestamp: Date = object['timestamp'];

					// sync method; no need to await
					PiKiosk.setSystemTime(timestamp);
				}
		);
	}
}
