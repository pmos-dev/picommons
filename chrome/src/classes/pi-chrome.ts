import * as child_process from 'child_process';

import * as rimraf from 'rimraf';

import { CommonsFile } from 'nodecommons-file';

export class PiChrome {
	private chrome: child_process.ChildProcess|undefined = undefined;
	
	constructor(
			private app: string = 'chromium-browser',
			private url: string = 'http://localhost:1234/embedded',
			private profile: string = '/home/pi/chromiumprofile'
	) {}
	
	private async doWipeCache(): Promise<void> {
		return new Promise((resolve: () => void, _: (reason: any) => void): void => {
			rimraf(
					`${this.profile}/Default/Cache`,
					(): void => {
						resolve();
					}
			);
		});
	}
	
	public async launch(
			wipeCache: boolean = false
	): Promise<boolean> {
		if (this.chrome !== undefined) throw new Error('Chrome already launched');

		try {
			if (
					wipeCache
					&& CommonsFile.exists(this.profile)
					&& CommonsFile.exists(`${this.profile}/Default/Cache`)
			) {
				// wipe cache, as it causes problems with different Angular apps having the same index.html file
				await this.doWipeCache();
			}
			
			this.chrome = child_process.spawn(
					this.app,
					[
							this.url,
							`--user-data-dir=${this.profile}`,
							'--kiosk',
							'--aggressive-cache-discard',
							'--disable-application-cache',
							'--disable-notifications'
					],
					{
							// tslint:disable-next-line:object-literal-sort-keys
							stdio: 'ignore',
							detached: true
					}
			);
			return true;
		} catch (e) {
			return false;
		}
	}
	
	public kill(): void {
		if (this.chrome === undefined) throw new Error('Chrome not started');

		this.chrome.kill();
		this.chrome = undefined;
	}
}
