import * as child_process from 'child_process';

import { CommonsDate } from 'tscommons-core';
import { CommonsType } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsFile } from 'nodecommons-file';
import { CommonsConfigFile } from 'nodecommons-config';

import { ICommonsCredentials } from 'nodecommons-database';

import { ECommonsDatabaseEngine } from 'nodecommons-database';

import { PiSocketIoHostServer } from 'picommons-kiosk';

import { TPiUpdateConfig } from '../types/tpi-update-config';
import { EPiUpdateType } from '../enums/epi-update-type';
import { IPiUpdateVersion, isIPiUpdateVersion } from '../interfaces/ipi-update-version';
import { PiRemoteUpdateService } from '../services/pi-remote-update.service';

import { PiDatabase } from './pi-database';

const TEMP_UNPACK_PATH: string = '/run/shm';

export class PiUpdate {
	private updateConfig: TPiUpdateConfig;
	private piDatabase?: PiDatabase;
	
	constructor(
			private config: CommonsConfigFile,
			private hostServer: PiSocketIoHostServer,
			databaseType?: ECommonsDatabaseEngine,
			credentials?: ICommonsCredentials
	) {
		this.updateConfig = config.getObject('update') as TPiUpdateConfig;
		
		if (databaseType && credentials) this.piDatabase = new PiDatabase(databaseType, credentials);
	}
	
	private async install(payload: Buffer): Promise<boolean> {
		const tempdir: string = CommonsDate.dateToYmdHis(new Date()).replace(/[- :]/g, '');
		
		CommonsFile.mkdir(`${TEMP_UNPACK_PATH}/${tempdir}`);
		child_process.execSync(`tar xf - --bzip2 --directory=${TEMP_UNPACK_PATH}/${tempdir}`, { input: payload });
		if (!CommonsFile.isDirectory(`${TEMP_UNPACK_PATH}/${tempdir}/${this.updateConfig.software}`)) {
			// unpack failed or is not a valid package we can understand
			return false;
		}

		child_process.execSync(`rsync -av ${TEMP_UNPACK_PATH}/${tempdir}/${this.updateConfig.software}/ ${this.updateConfig.destination}`);
		
		if (this.piDatabase) {
			const rebuildScriptPath: string = `${this.updateConfig.destination}/sql/rebuild.sql`;
			if (CommonsFile.exists(rebuildScriptPath)) {
				if (!this.piDatabase.runOnce(rebuildScriptPath)) return false;
			}
		}
		
		if (this.updateConfig.npmInstall) child_process.execSync(`npm install`, { cwd: `${this.updateConfig.destination}/` });
		CommonsFile.sync();

		return true;
	}
	
	private storeNewVersion(version: IPiUpdateVersion): void {
		// reload just in case other changes have been made since instantiation
		const activeUpdateConfig: TPiUpdateConfig = this.config.getObject('update') as TPiUpdateConfig;
		activeUpdateConfig.current = version.version;
		this.config.setObject('update', this.updateConfig);

		this.config.save();

		this.updateConfig = this.config.getObject('update') as TPiUpdateConfig;
	}
	
	private listMediaUpdates(media: string): IPiUpdateVersion[] {
		const available: IPiUpdateVersion[] = [];
		
		for (const file of CommonsFile.listFiles(`${this.updateConfig.mediaPath}/${media}`)) {
			const regex: RegExp = new RegExp(`^${this.updateConfig.software}_([0-9]{1,4})\.tar\.bz2$`);
			const result: RegExpExecArray|null = regex.exec(file);
			if (!result) continue;

			available.push({
					software: this.updateConfig.software,
					version: parseInt(result[1], 10),
					timestamp: CommonsFile.lastModified(`${this.updateConfig.mediaPath}/${media}/${file}`)
			});
		}
		
		return available;
	}
	
	public useOnUpdate(): void {
		this.hostServer.on('update/type', (_: any): EPiUpdateType => {
			CommonsOutput.info('Update type');
			return this.updateConfig.type;
		});

		this.hostServer.on('update/current', (_: any): number => {
			CommonsOutput.info('Update current');
			return this.updateConfig.current;
		});

		switch (this.updateConfig.type) {
			case EPiUpdateType.REMOTE:
				this.useOnRemoteUpdate();
				break;
			case EPiUpdateType.MEDIA:
				this.useOnMediaUpdate();
				break;
		}
	}
	
	private useOnRemoteUpdate(): void {
		const updateService: PiRemoteUpdateService = new PiRemoteUpdateService(
				this.config,
				this.updateConfig.software
		);
	
		this.hostServer.onAsync('update/remote', async (_: any): Promise<TEncodedObject|undefined> => {
			CommonsOutput.info('Update remote (latest)');
			try {
				const version: IPiUpdateVersion|undefined = await updateService.latest();
				if (!version) return undefined;
	
				return CommonsType.encodePropertyObject(version);
			} catch (e) {
				CommonsOutput.error(e);
				throw e;
			}
		});
		
		this.hostServer.onAsync('update/install', async (encoded: TEncodedObject): Promise<boolean> => {
			CommonsOutput.info('Update install (download)');
			
			const version: IPiUpdateVersion = CommonsType.decodePropertyObject(encoded) as IPiUpdateVersion;
			if (!isIPiUpdateVersion(version)) return false;
			
			try {
				const payload: Buffer = await updateService.download(version);
				if (!payload) return false;
				if (!(await this.install(payload))) return false;
		
				this.storeNewVersion(version);
				
				return true;
			} catch (e) {
				CommonsOutput.error(e);
				return false;
			}
		});
	}

	private useOnMediaUpdate(): void {
		this.hostServer.on('update/source', (_: any): string[] => {
			CommonsOutput.info('List sources', this.updateConfig.mediaPath);
			try {
				if (this.updateConfig.mediaPath === undefined) return [];
				return CommonsFile.listDirectories(this.updateConfig.mediaPath);
			} catch (e) {
				this.hostServer.exception('Error listing directories');
				return [];
			}
		});

		this.hostServer.onAsync('update/media', async (media: string): Promise<TEncodedObject|undefined> => {
			CommonsOutput.info('Update media (latest)');
			
			const available: IPiUpdateVersion[] = this.listMediaUpdates(media)
					.sort((a: IPiUpdateVersion, b: IPiUpdateVersion): number => {
						if (a.version < b.version) return -1;
						if (a.version > b.version) return 1;
						return 0;
					})
					.reverse();
		
			if (available.length === 0) return undefined;

			return CommonsType.encodePropertyObject(available[0]);
		});
		
		this.hostServer.onAsync('update/install', async (encoded: TEncodedObject): Promise<boolean> => {
			CommonsOutput.info('Update install (media)');
			
			if (!CommonsType.hasProperty(encoded, 'media') || 'string' !== typeof encoded.media) return false;
			const media: string = encoded.media;
			
			const version: IPiUpdateVersion = CommonsType.decodePropertyObject(encoded) as IPiUpdateVersion;
			if (!isIPiUpdateVersion(version)) return false;

			const available: IPiUpdateVersion[] = this.listMediaUpdates(media)
					.filter((v: IPiUpdateVersion) => v.version === version.version);
			if (available.length === 0) return false;
	
			const filename: string = `${this.updateConfig.mediaPath}/${media}/${version.software}_${version.version}.tar.bz2`;
			CommonsOutput.info(`Updating from ${filename}`);
	
			try {
				const payload: Buffer|undefined = await CommonsFile.readBinaryFile(filename);
				if (!payload) return false;
				
				CommonsOutput.info('Read payload successfully');
				if (!(await this.install(payload))) return false;
		
				this.storeNewVersion(version);
				
				return true;
			} catch (e) {
				CommonsOutput.error(e);
				return false;
			}
		});
	}
}
