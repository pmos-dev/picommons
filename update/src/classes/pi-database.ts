import * as child_process from 'child_process';

import { CommonsFile } from 'nodecommons-file';
import { CommonsOutput } from 'nodecommons-cli';

import { ICommonsCredentials } from 'nodecommons-database';
import { ECommonsDatabaseEngine } from 'nodecommons-database';
import { CommonsPostgresService } from 'nodecommons-database-postgres';

export class PiDatabase {
	constructor(
			private type: ECommonsDatabaseEngine,
			private credentials: ICommonsCredentials
	) {
	}
	
	public run(script: string): boolean {
		CommonsOutput.doing(`Attempting to run database script: ${script}`);
		try {
			switch (this.type) {
				case ECommonsDatabaseEngine.POSTGRES:
					const cn: string = CommonsPostgresService.toUrl(this.credentials);
					child_process.execSync(`psql "${cn}" < "${script}"`);
					
					break;
				default:
					throw new Error('Unsupported database type');
			}

			CommonsOutput.success();
			return true;
		} catch (ex) {
			CommonsOutput.fail();
			console.log(ex);
			return false;
		}
	}

	public runOnce(script: string): boolean {
		if (!this.run(script)) return false;

		CommonsFile.rm(script);

		return true;
	}
}
