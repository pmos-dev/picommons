import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';

export interface IPiUpdateVersion {
	software: string;
	version: number;
	timestamp: Date;
}

export function isIPiUpdateVersion(test: any): test is IPiUpdateVersion {
	if (!CommonsType.isObject(test)) return false;
	
	const attempt: TPropertyObject = test as IPiUpdateVersion;
	
	if (!CommonsType.hasProperty(attempt, 'software') || 'string' !== typeof attempt.software) return false;
	if (!CommonsType.hasProperty(attempt, 'version') || 'number' !== typeof attempt.version) return false;
	if (!CommonsType.hasProperty(attempt, 'timestamp') || !CommonsType.isDate(attempt.timestamp)) return false;

	return true;
}
