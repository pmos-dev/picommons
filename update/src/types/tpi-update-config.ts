import { EPiUpdateType } from '../enums/epi-update-type';

export type TPiUpdateConfig = {
		type: EPiUpdateType;
		software: string;
		current: number;
		destination: string;
	
		url?: string;
		key?: string;
		
		mediaPath?: string;
		npmInstall?: boolean;
};
