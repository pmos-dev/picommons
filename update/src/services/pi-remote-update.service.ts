import { CommonsType } from 'tscommons-core';
import { CommonsConfig } from 'tscommons-config';
import { TPropertyObject } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';

import { CommonsKeyRestClientService } from 'nodecommons-rest';

import { ECommonsDataEncoding } from 'nodecommons-rest';

import { IPiUpdateVersion, isIPiUpdateVersion } from '../interfaces/ipi-update-version';
import { TPiUpdateConfig } from '../types/tpi-update-config';

export class PiRemoteUpdateService extends CommonsKeyRestClientService {
	private static getRestUrl(
			config: CommonsConfig
	): string {
		const c: TPiUpdateConfig = config.getObject('update') as TPiUpdateConfig;
		return CommonsType.assertString(c.url);
	}

	private static getKey(
			config: CommonsConfig
	): string {
		const c: TPiUpdateConfig = config.getObject('update') as TPiUpdateConfig;
		return CommonsType.assertString(c.key);
	}

	constructor(
		config: CommonsConfig,
		private software: string,
		timeout: number = 15000
	) {
		super(
				PiRemoteUpdateService.getRestUrl(config),
				ECommonsDataEncoding.FORM,
				PiRemoteUpdateService.getKey(config)
		);

		super.setTimeout(timeout);
	}
	
	public async latest(): Promise<IPiUpdateVersion|undefined> {
		const result: any = await super.get<any>(`softwares/${this.software}/versions/latest`);
		if (!CommonsType.isEncodedObject(result)) return undefined;
		
		const decoded: TPropertyObject = CommonsType.decodePropertyObject(result);
		CommonsOutput.info(decoded);
		
		if (!isIPiUpdateVersion(decoded)) return undefined;
		
		return decoded;
	}
	
	public async download(version: IPiUpdateVersion): Promise<Buffer> {
		try {
			return await super.get<Buffer>(`softwares/${version.software}/versions/${version.version}/download`, {}, true);
		} catch (e) {
			CommonsOutput.error(e);
			throw e;
		}
	}
}
